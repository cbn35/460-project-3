import java.net.*;
import java.io.*;


class DatagramServer {
    public static void main(String[] args) throws Exception{
        int port = Integer.parseInt(args[0]);
        DatagramSocket server = new DatagramSocket(port);

        while(true) {
            byte[] data = new byte[1024];
            DatagramPacket receive = new DatagramPacket(data, data.length);
            server.receive(receive);
            String revievedString = new String(receive.getData());

            InetAddress sendIP = receive.getAddress();
            int sendPort = receive.getPort();

            DatagramPacket send = new DatagramPacket(data, data.length, sendIP, sendPort);
            server.send(send);

        }

    }

}
